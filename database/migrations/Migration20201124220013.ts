import { Migration } from '@mikro-orm/migrations';

export class Migration20201124220013 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `users` (`id` integer not null, `created_at` datetime not null, `updated_at` datetime null, `name` varchar not null, primary key (`id`));');

    this.addSql('create table `participants` (`id` integer not null, `created_at` datetime not null, `updated_at` datetime null, `status` text check (`status` in (\'maybe\', \'coming\', \'undecided\', \'not-coming\')) not null default \'undecided\', primary key (`id`));');

    this.addSql('create table `events` (`id` integer not null, `created_at` datetime not null, `updated_at` datetime null, `title` varchar not null, `description` varchar not null, `start_date` datetime not null, `end_date` datetime not null, `status` text check (`status` in (\'draft\', \'published\')) not null default \'draft\', primary key (`id`));');

    this.addSql('create table `event_participant` (`event_id` integer not null, `participant_id` integer not null, primary key (`event_id`, `participant_id`));');
    this.addSql('create index `event_participant_event_id_index` on `event_participant` (`event_id`);');
    this.addSql('create index `event_participant_participant_id_index` on `event_participant` (`participant_id`);');

    this.addSql('alter table `participants` add column `user_id` integer null;');
    this.addSql('create index `participants_user_id_index` on `participants` (`user_id`);');
    this.addSql('create unique index `participants_user_id_unique` on `participants` (`user_id`);');
  }

}
