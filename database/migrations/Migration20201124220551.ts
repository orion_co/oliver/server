import { Migration } from '@mikro-orm/migrations';

export class Migration20201124220551 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table `events` add column `creator_id` integer null;');
    this.addSql('create index `events_creator_id_index` on `events` (`creator_id`);');
    this.addSql('create unique index `events_creator_id_unique` on `events` (`creator_id`);');
  }

}
