import dotenv from 'dotenv';
import packageFile from '../../package.json';

const envFound = dotenv.config();
if (envFound.error) {
  throw new Error("Couldn't find .env file️");
}

const ENV: string = process.env.NODE_ENV || 'production';
const DEBUG = Boolean(process.env.DEBUG && process.env.DEBUG === 'true');

export default {

  debug: DEBUG,

  version: packageFile.version,

  dbName: `database/db.${ENV}.sqlite`,

  name: process.env.APP_NAME,

  env: ENV,

  port: process.env.APP_PORT || '3001',
  host: process.env.APP_HOST || 'localhost',

  log: {
    level: ENV === 'test' ? [] as never : (process.env.LOG_LEVEL || 'error'),
  },
};
