import { TsMorphMetadataProvider } from '@mikro-orm/reflection';
import { SqlHighlighter } from '@mikro-orm/sql-highlighter';
import { Options } from '@mikro-orm/core';
import config from './';

const ormConfig: Options = {
  entities: [
    './dist/models/BaseModel.js',
    './dist/models/**/*.*',
  ],
  entitiesTs: [
    './src/models/BaseModel.ts',
    './src/models/**/*.*',
  ],
  dbName: config.dbName,
  type: 'sqlite',
  metadataProvider: TsMorphMetadataProvider,
  debug: config.debug,
  logger: config.debug ? i => console.debug(i) : undefined, // eslint-disable-line no-console
  highlighter: new SqlHighlighter(),
  migrations: {
    tableName: 'migrations',
    path: 'database/migrations',
    pattern: /^[\w-]+\d+\.(js|ts)$/,
    transactional: true,
    disableForeignKeys: true,
    allOrNothing: true,
    emit: 'ts',
  },
};

export {
  ormConfig,
};
