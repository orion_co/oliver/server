import { MikroORM } from '@mikro-orm/core';
import { SqliteDriver } from '@mikro-orm/sqlite';
import { ormConfig } from './mikro-orm-config';

export const orm = async (config?: any): Promise<MikroORM<SqliteDriver>> => {
  return await MikroORM.init<SqliteDriver>(config || ormConfig);
};

let instance: MikroORM<SqliteDriver>;
let semaphore = false;
export const getCachedOrmInstance = async (): Promise<MikroORM<SqliteDriver>> => {
  if (!instance && !semaphore) {
    semaphore = true;
    instance = await orm(ormConfig);
  }

  return instance;
};
