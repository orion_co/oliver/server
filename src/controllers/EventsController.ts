import { Request } from '@hapi/hapi';
import { ResponseToolkitOrm } from '../server';
import { Event } from '../models/Event';
import { EventEmitter } from '../utils/EventEmitter';

const index = async (request: Request, h: ResponseToolkitOrm) => {
  const orm = await h.getEntityManager();

  return await orm.find(Event, {}, { populate: ['participants'] });
};

const show = async (request: Request, h: ResponseToolkitOrm) => {
  const orm = await h.getEntityManager();

  return await orm.findOneOrFail(Event, request.params.id, { populate: ['participants', 'participants.user'] });
};

const create = async (request: Request, h: ResponseToolkitOrm) => {
  const orm = await h.getEntityManager();
  const payload: any = request.payload;

  const event = new Event(
    payload.title,
    payload.description,
    payload.start_date,
    payload.end_date
  );

  await orm.persistAndFlush(event);

  EventEmitter.emit('newEventCreated', { eventId: event.id });

  return event;
};

const queryByTitle = async (request: Request, h: ResponseToolkitOrm) => {
  const orm = await h.getEntityManager();
  let query = {};

  if (request.query.title) {
    const title = new RegExp(request.query.title);
    query = { title };
  }

  return await orm.find(Event, query, { populate: ['participants'] });
};

const updateParticipation = async (request: Request, h: ResponseToolkitOrm) => {
  const orm = await h.getEntityManager();

  const { id, participantId }: any = request.params;
  const { status }: any = request.payload;

  const event = await orm.findOneOrFail(Event, id, { filters: false, populate: ['participants'] });
  for (const participant of event.participants) {
    if (participant.id === Number(participantId)) {
      participant.status = status;
      await orm.persistAndFlush(participant);
      return participant;
    }
  }

  return;
};

export {
  index,
  show,
  create,
  updateParticipation,
  queryByTitle,
};
