import { BaseModel } from './BaseModel';
import { Collection, Filter, Entity, Enum, ManyToMany, Property, ManyToOne } from '@mikro-orm/core';
import { EventParticipant } from './EventParticipant';
import { User } from './User';
import moment from 'moment';

export enum EventStatus {
  DRAFT = 'draft',
  PUBLISHED = 'published'
}

export enum FilterNames {
  ONLY_EXPIRED = 'onlyExpired',
  ONLY_UPCOMING = 'onlyUpcoming',
  ONLY_DRAFT = 'onlyDraft',
  ONLY_PUBLISHED = 'onlyPublished'
}

@Filter({ name: FilterNames.ONLY_EXPIRED, cond: { end_date: { $lt: moment().format('x') } } })
@Filter({ name: FilterNames.ONLY_UPCOMING, cond: { start_date: { $gte: moment().format('x') } }, default: true })
@Filter({ name: FilterNames.ONLY_DRAFT, cond: { status: { $eq: EventStatus.DRAFT } } })
@Filter({ name: FilterNames.ONLY_PUBLISHED, cond: { status: { $eq: EventStatus.PUBLISHED } }, default: true })
@Entity({ tableName: 'events' })
export class Event extends BaseModel {

  @Property({ type: 'varchar' })
  public title: string;

  @Property({ type: 'varchar' })
  public description: string;

  @Property({ type: 'date' })
  public start_date: Date;

  @Property({ type: 'date' })
  public end_date: Date;

  @Enum({ default: EventStatus.DRAFT })
  public status: EventStatus;

  @Property({ type: 'bigint', unsigned: true })
  public creator_id?: number;

  @ManyToMany({
    entity: () => EventParticipant,
    pivotTable: 'event_participant',
    joinColumn: 'event_id',
    inverseJoinColumn: 'participant_id',
    eager: true,
  })
  public participants = new Collection<EventParticipant>(this);

  @ManyToOne(() => User, { nullable: false, unique: false })
  public creator!: User;

  public constructor(title: string, description: string, start_date: Date, end_date: Date, status: EventStatus = EventStatus.DRAFT) {
    super();

    this.title = title;
    this.description = description;
    this.start_date = start_date;
    this.end_date = end_date;
    this.status = status;
  }

}
