import { BaseModel } from './BaseModel';
import { Entity, OneToOne, Enum } from '@mikro-orm/core';
import { User } from './User';

export enum ParticipantStatus {
  MAYBE = 'maybe',
  COMING = 'coming',
  UNDECIDED = 'undecided',
  NOT_COMING = 'not-coming',
}

@Entity({ tableName: 'participants' })
export class EventParticipant extends BaseModel {

  @Enum({ default: ParticipantStatus.UNDECIDED })
  public status: ParticipantStatus;

  @OneToOne(() => User)
  public user: User;

  public constructor(status: ParticipantStatus, user: User) {
    super();

    this.status = status;
    this.user = user;
  }

}
