import { BaseModel } from './BaseModel';
import { Event } from './Event';
import { Collection, OneToMany, Entity, Property } from '@mikro-orm/core';

@Entity({ tableName: 'users' })
export class User extends BaseModel {

  @Property({ type: 'varchar' })
  public name: string;

  @Property({ type: 'varchar' })
  public username: string;

  @OneToMany({ mappedBy: (event: Event) => event.creator })
  public eventsCreated = new Collection<Event>(this);

  public constructor(name: string, username: string) {
    super();

    this.name = name;
    this.username = username;
  }

}
