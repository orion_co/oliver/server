import {
  index as EventsIndex,
  show as EventsShow,
  create as EventsCreate,
  updateParticipation,
  queryByTitle as EventsQuery,
} from '../controllers/EventsController';
import Joi, { ErrorReport } from 'joi';
import { ParticipantStatus } from '../models/EventParticipant';

export const EventSchema = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
  start_date: Joi.date().required(),
  end_date: Joi.date().min(Joi.ref('start_date')).required().error((errors: ErrorReport[]): any => {
    return errors.map((error: ErrorReport) => {
      switch (error.code) {
        case 'date.min':
          error.message = `"end_date" must come after "start_date".`;
      }

      return error;
    });
  }),
}).options({ stripUnknown: true });


export const EventParticipantSchema = Joi.object({
  status: Joi.string().valid(...Object.values(ParticipantStatus)),
}).options({ stripUnknown: true });

export default {
  name: 'ApiRoutes',
  register: async (server: any, options: any) => {
    server.route([
      {
        method: 'GET',
        path: '/events',
        config: {
          id: 'events.index',
          handler: EventsIndex,
        },
      },
      {
        method: 'POST',
        path: '/events',
        config: {
          id: 'events.create',
          handler: EventsCreate,
          validate: {
            payload: EventSchema,
          },
        },
      },
      {
        method: 'GET',
        path: '/events/query',
        config: {
          id: 'events.query',
          handler: EventsQuery,
        },
      },
      {
        method: 'GET',
        path: '/events/{id}',
        config: {
          id: 'events.show',
          handler: EventsShow,
        },
      },
      {
        method: 'PUT',
        path: '/events/{id}/participants/{participant_id}/status',
        config: {
          id: 'events.participant.update',
          handler: updateParticipation,
          validate: {
            payload: EventParticipantSchema,
          },
        },
      },
    ]);
  },
};
