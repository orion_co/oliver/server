'use strict';

process.title = 'server';

import { Server, Request, ResponseToolkit, LogEvent, ResponseObject } from '@hapi/hapi';
import LoggerInstance from './services/logger';
import { getCachedOrmInstance as ormInstance } from './config/orm';
import routes from './routes';
import inert from '@hapi/inert';
import config from './config';
import Boom from '@hapi/boom';
import { EntityManager } from '@mikro-orm/core/EntityManager';

const HOST = config.host;
const HOST_PORT = config.port;

export interface ResponseToolkitOrm extends ResponseToolkit {
  getEntityManager(): Promise<EntityManager>;
}

const server = new Server({
  port: HOST_PORT,
  host: HOST,
  routes: {
    cors: {
      origin: ['*'],
      headers: ['Accept', 'Content-Type'],
      additionalHeaders: ['X-Requested-With'],
    },
    validate: {
      // TODO: Improve the validation error message response
      failAction: async (request, h, err: any) => {
        if (['development'].indexOf(config.env) > -1) {
          LoggerInstance.error(err);
          throw err;
        }

        throw Boom.badRequest(`Invalid payload: ${err.message}`);
      },
    },
  },
  router: {
    stripTrailingSlash: true,
  },
  debug: { request: ['*'] },
});

server.route({
  method: '*',
  path: '/{any*}',
  handler: (request: Request, h: ResponseToolkit) => {
    return h.response({ statusCode: 404, version: config.version, server: config.name, host: `${HOST}:${HOST_PORT}` }).code(404);
  },
});

const init = async (): Promise<Server> => {
  await server.initialize();
  return server;
};

const start = async (): Promise<void> => {
  const orm = await ormInstance();
  const getEM = {
    name: 'getEntityManager',
    async register(server: any, options: any) {
      const em = async () => {
        return await orm.em.fork(true, true);
      };

      server.decorate('toolkit', 'getEntityManager', em);
    },
  };

  await server.register([inert, getEM, routes], {
    routes: {
      prefix: '/api',
    },
  });

  await server.start();
  LoggerInstance.debug(`[${config.env}] Server running on ${config.host}:${config.port}`);
};

server.events.on('response', (request: Request) => {
  LoggerInstance.debug(`${config.host}:${config.port}: - - ${request.method.toUpperCase()} ${request.path} --> ${(request.response as ResponseObject).statusCode}`);
});

server.events.on('log', (event: LogEvent, tags: {[key: string]: true }) => {
  if (tags.error) {
    LoggerInstance.error(`${config.env}] Server error: ${event.error ? (event.error as any).message : 'unknown'}`);
  }
});

(async () => {
  await init();
  await start();
})().catch((error: Error) => {
  LoggerInstance.error(`${config.env}] Server error: ${error ? error.message : 'unknown'}`);
  process.exit();
});

export {
  server,
};
