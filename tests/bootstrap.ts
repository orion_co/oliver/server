import { MikroORM } from '@mikro-orm/core';
import { SqliteDriver } from '@mikro-orm/sqlite';
import { orm } from '../src/config/orm';

const getOrmInstance = async (): Promise<MikroORM<SqliteDriver>> => {
  return await orm();
};

const runFreshMigrations = async (orm: MikroORM): Promise<any[]> => {
  return await orm.getMigrator().up();
};

const tearDownOrm = async (orm: MikroORM) => {
  await orm.close(true);
  await orm.em.clear();
};

export {
  tearDownOrm,
  getOrmInstance,
  runFreshMigrations,
};
