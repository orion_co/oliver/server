import { ServerInjectOptions } from '@hapi/hapi';
import { MikroORM } from '@mikro-orm/core';
import { SqliteDriver } from '@mikro-orm/sqlite';
import { getOrmInstance, runFreshMigrations, tearDownOrm } from './bootstrap';
import { EventEmitter } from '../src/utils/EventEmitter';
const { server } = require('../src/server');
const fs = require('fs');

jest.setTimeout(80000);

let ormInstance: MikroORM<SqliteDriver>;

describe('EVENT API ROUTES', () => {

  // These are expensive before and after each test.
  // TODO: Improve DB migration/transactions.
  beforeEach(async () => {
    await fs.truncate('database/db.test.sqlite', () => true);

    ormInstance = await getOrmInstance();
    await runFreshMigrations(ormInstance);
  });

  afterEach(async () => {
    // await tearDownOrm(ormInstance);
    return server.stop();
  });

  test('GET /events returns expected list of events', async function () {
    const options: ServerInjectOptions = {
      method: 'GET',
      url: '/api/events',
    };
    const data = await server.inject(options);

    expect(data.statusCode).toBe(200);
    expect(data.result).toEqual([]);
  });

  test('POST /events throws validation error', async function () {
    const options: ServerInjectOptions = {
      method: 'POST',
      url: '/api/events',
      payload: {
        name: 'foo',
      },
    };
    const data = await server.inject(options);

    expect(data.statusCode).toBe(400);
    expect(data.result.message.includes('Invalid payload')).toBeTruthy();
  });

  test('POST /events throws validation error when end date is before start date', async function () {
    const options: ServerInjectOptions = {
      method: 'POST',
      url: '/api/events',
      payload: {
        title: 'foo',
        description: 'bar',
        start_date: '2020-01-11',
        end_date: '2020-01-10',
      },
    };
    const data = await server.inject(options);

    expect(data.statusCode).toBe(400);
    expect(data.result.message.includes('"end_date" must come after "start_date"')).toBeTruthy();
  });

  test('POST /events returns created event', async function () {
    spyOn(EventEmitter, 'emit');

    const options: ServerInjectOptions = {
      method: 'POST',
      url: '/api/events',
      payload: {
        title: 'foo',
        description: 'bar',
        start_date: '2020-01-11',
        end_date: '2020-01-11',
      },
    };
    const data = await server.inject(options);
    const event = data.result;

    expect(EventEmitter.emit).toHaveBeenCalledTimes(1);
    expect(EventEmitter.emit).toHaveBeenCalledWith('newEventCreated', { eventId: event.id });
    expect(data.statusCode).toBe(200);
    expect(event.title).toEqual('foo');
    await expect(ormInstance.em.getConnection().execute(`SELECT id FROM events WHERE id = ${event.id}`)).resolves.toEqual([{ id: event.id }]);
  });

});
